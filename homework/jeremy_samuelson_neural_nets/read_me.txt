git_url:	https://jeremysamuelson@bitbucket.org/jeremysamuelson/data_science.git

issues:		There were no specific instructions posted for this assignment, so I just found a dataset on breast cancer
		and implemented a simple neural network for classification.

results:	I implemented a simple neural network
