git_url:	https://jeremysamuelson@bitbucket.org/jeremysamuelson/data_science.git

issues:		This was the last assignment I did tonight, so I didn't have a lot of time to finish it.

results:	I was able to answer the simpler problems, but didn't have enough time to implement code
		on the last two.
