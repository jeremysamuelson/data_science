git_url:	https://jeremysamuelson@bitbucket.org/jeremysamuelson/data_science.git

issues:		In the exercise that involved tinkering with different values of k, it was my intention to make
		a bunch of plots showing how the decision barrier changed for different k.  I was able to plot all
		of the points colored by category, but I couldn't figure out how to plot the decision barrier.  I just
		ended up scrapping that whole track, because I'm running out of time and I still need to do the gradient
		descent homework.

results:	It's done, mostly.  I feel like I understand k-nearest neighbors classification better.  I already had a
		pretty good understanding of bias vs. variance.  This was a nice refresher.
