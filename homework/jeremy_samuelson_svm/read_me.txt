git_url:	https://jeremysamuelson@bitbucket.org/jeremysamuelson/data_science.git

issues:		No real issues.  I was able to draw the decision boundary, but mine didn't look as clean as the one in
		the example plots that were already in the notebook.  Not sure why that is.

results:	I'd never dealt with an SVM model before.  I feel like I'm at a data analyst level of comprehension on
		this subject now.  That is, I know how to use sklearn to implement an SVM model, but I don't understand
		it on a deep level.  I'll have to keep studying this.
