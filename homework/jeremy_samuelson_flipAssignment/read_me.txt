git_url:	https://jeremysamuelson@bitbucket.org/jeremysamuelson/data_science.git

issues:		I was first trying to do this through some clever use of the string.split() method, but then this method
		came to me and seemed much easier.

results:	I was actually able to pass ALL of the test cases.  I've been practicing on codefights.com, and I think
		it's helping.  I want to be a really strong coder.
