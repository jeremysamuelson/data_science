
# coding: utf-8

# In[12]:


def messageFromBinaryCode(code):
    #no join
    #no list comprehension
    return "".join([chr(int(code[i:i+8], 2)) for i in range(0,len(code),8)])


# In[13]:


messageFromBinaryCode("0100100001100101011011000110110001101111")


# In[14]:


def messageFromBinaryCode(code):
    return reduce(lambda x,y: x+y, map(lambda x: chr(int(code[x:x+8], 2)), list(range(0,len(code),8))))


# In[15]:


messageFromBinaryCode("0100100001100101011011000110110001101111")

