git_url:	https://jeremysamuelson@bitbucket.org/jeremysamuelson/data_science.git

issues:		None.  This was by far the easiest assignment we had

results:	Success.  The map-reduce method really is the correct way to think about this.  I'm a little embarrased
		that it's not what we did, but I think what we came up with on the spot gets the job done.
