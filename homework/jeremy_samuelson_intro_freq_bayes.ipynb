{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Question 1:\n",
    "We run a two-sample t-test for equal means, with α = 0.05, and obtain a p-value of 0.04.\n",
    "What are the odds that the two samples are drawn from distributions with the same mean?\n",
    "(a) 19/1 (b) 1/19 (c) 1/20 (d) 1/24 (e) unknown"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(d) $\\frac{1}{24}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Question 2:\n",
    "When running a series of trials we need a rule on when to stop. Two common rules are:\n",
    "\n",
    "1. Run exactly n trials and stop.\n",
    "2. Run trials until you see a certain result and then stop.\n",
    "\n",
    "In this example we’ll consider two coin tossing experiments.\n",
    "\n",
    "Experiment 1: Toss the coin exactly 6 times and report the number of heads.\n",
    "\n",
    "Experiment 2: Toss the coin until the first tails and report the number of heads.\n",
    "\n",
    "Jon is worried that his coin is biased towards heads, so before using it in class he tests it for fairness. He runs an experiment and reports to Jerry that his sequence of tosses was HHHHHT. But Jerry is only half-listening, and he forgets which experiment Jon ran to produce the data.\n",
    "\n",
    "\n",
    "* What is the Frequentist approach?\n",
    "\n",
    "* What is the Bayesian approach?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Frequentist approach:\n",
    "\n",
    "Frequentists think _deductively_, so at a high level, the reasoning is: \"If the true probability of flipping heads is $p$, then my samples will probably look like this.\"  In other words, to a frequentist the underlying parameters are fixed and the observed data are random.\n",
    "\n",
    "If Jon is worried that his coin is biased toward heads, the frequentist approach to making a conclusion is as follows:\n",
    "\n",
    "Null hypothesis: The coin is not biased toward heads $(p \\leq 0.5)$.\n",
    "\n",
    "Alternative hypothesis: The coin is biased toward heads $(p > 0.5)$.\n",
    "\n",
    "We assume the null hypothesis to be true, and we test at the edge case $(p = 0.5)$.  Since we are dealing with two very well known distributions (Experiment 1 yields a Binomial distribution and Experiment 2 yields a Geometric distribution).  We know both the mean $(\\mu)$ and standard deviation $(\\sigma)$ of the underlying distribution under the null hypothesis.  This means we can execute a Z-test.  The Z-test invokes the Central Limit Theorem, which tells us that if we were to perform either experiment a large number of times, and record the sample mean each time, the sample means will take on the following distribution:\n",
    "\n",
    "$${\\overline X} \\sim \\mathcal{N}\\left(\\mu, \\frac{\\sigma}{\\sqrt{n}}\\right) $$\n",
    "\n",
    "Normalizing this distribution by applying the following transformation\n",
    "\n",
    "$$Z = \\frac{{\\overline X} - \\mu_{{\\overline X}}}{\\sigma_{{\\overline X}}}$$\n",
    "\n",
    "gives us the Z distribution.  We can now perform our Z-test (by looking up the Z value of our observed data) to determine the probability of observing the observed sample mean under the null hypothesis.  If the probability is below some pre-determined threshold ($\\alpha < 0.05$ in many cases, but this varies wildly by field), then we have met the criteria to reject our null hypothesis in favor of our alternative hypothesis.  If the probability of observing our sample mean is not within the pre-determined range, then we fail to reject our null hypothesis.\n",
    "\n",
    "In the event that we reject our null hypothesis, we may repeatedly sample from our sample to bootstrap a Confidence Interval for the true probability value of getting heads, but we can't make any exact conclusions as to the true value.\n",
    "\n",
    "The inherent flaws with the frequentist approach are:\n",
    "\n",
    "1. $p$-values measured against some sample statistic change with the sample size.  This means two different teams working with the same data may get different $p$-values, which is not desirable.\n",
    "2. The Confidence Interval for the true parameter value also depends heavily on sample size.\n",
    "3. Confidence Intervals are not probability distributions, therefore they do not provide the most probable value for the true parameter in question.\n",
    "\n",
    "\n",
    "#### Bayesian approach:\n",
    "\n",
    "Bayesian statisticians think _inductively_, so at a high level, the reasoning is: \"My sample came out like this, so the true parameter is probably $\\theta$.\"  In other words, to a Bayesian statistician the data are fixed and the underlying parameters are random with associated distribution functions.\n",
    "\n",
    "Let the underlying parameter be $\\theta$, and let the observed event be $E$.  What we are trying to determine is the probability of a given parameter value, given our observed data $\\left(P\\left(\\theta\\thinspace|\\thinspace E\\right)\\right)$.  In the context of Jon's concern, we are asking \"Given the outcome $(E)$, what is the probability that the coin is biased toward heads $(\\theta > 0.5)$?  To answer this, we need to invoke Bayes Theorem:\n",
    "\n",
    "$$P\\left(\\theta\\thinspace|\\thinspace E\\right) = \\frac{P\\left(E\\thinspace|\\thinspace\\theta\\right)P(\\theta)}{P(E)}$$\n",
    "\n",
    "Here, $P(\\theta)$ is the prior (i.e. the strength of our belief in the coin's fairness before the experiment).  It is perfectly fine to believe the coin has any degree of fairness going into the experiment $\\left(0 \\leq \\theta \\leq 1 \\right)$.\n",
    "\n",
    "$P(E\\thinspace|\\thinspace\\theta)$ is the probability of observing the data we observed, given our prior belief for $\\theta$.\n",
    "\n",
    "$P(E)$ is the evidence.  This is the probability of the data as determined by summing (for discrete variables) or integrating (for continuous variables) across all possible values of $\\theta$ weighted by how strongly we believe in those particular values.\n",
    "\n",
    "To define everything correctly, we need two mathematical models before hand, one that defines our prior beliefs and one that defines the likelihood of our data, given our beliefs.\n",
    "\n",
    "If we don't really have any prior beliefs, then we can choose an \"uninformative prior.\"  In this case, we can choose the $B$-distribution.\n",
    "\n",
    "For the model that defines the likelihood of our data, we simply use the Bernoulli Likelihood Function, which has the general definition:\n",
    "\n",
    "$$P\\left(x_1, x_2, ..., x_n \\thinspace|\\thinspace\\theta\\right) = \\prod_{i=1}^{n} P(x_i\\thinspace|\\thinspace\\theta)$$\n",
    "\n",
    "Again, we are very lucky to be dealing with such well understood distributions in Experiments 1 and 2, because the likelihood functions are readily available.\n",
    "\n",
    "Once our posterior distribution $P(\\theta\\thinspace|\\thinspace E)$ is calculated, we need to determine how we test for the significance of our result.  For this, we calculate a Bayes Factor by comparing the Null and Alternative hypotheses from our prior and posterior beliefs.  A common rejection criterion is BF < 1/10, but like the $p$-value this can also vary from field to field.  The BF is calculated thusly:\n",
    "\n",
    "$$BF = \\frac{P(\\theta \\in null \\thinspace|\\thinspace E)}{P(\\theta \\in alt\\thinspace|\\thinspace E)} / \\frac{P(\\theta \\in null)}{P(\\theta \\in alt)}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Question 3:\n",
    "Jerry steals a JP Licks token and asks Jon to perform a test at signiﬁcance level α = 0.05 to investigate whether the coin is fair or biased toward tails (the side that says ‘Token’). Jon records the following data ->\n",
    "\n",
    "THTTHTTTTTTH\n",
    "\n",
    "a. Ericka believes that Jon's intention was to count the number of heads in twelve flips. Let's call this experiment 1. Compute the rejection region and p-value. Show the distribution and rejection region. What does Erika conclude?\n",
    "\n",
    "b. Jerry actually told Jon to count the number of heads in 100 ﬂips, so Jerry ﬁgures that Jon must have gotten bored and quit right after the 12th ﬂip. Strictly speaking, can Jerry compute a p-value from Jon’s partial data? Why or why not?\n",
    "\n",
    "c. Let’s reexamine the same data from the Bayesian perspective. What is the likelihood function for in Experiment 1? Given the prior Beta(n,m), ﬁnd the posterior."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "#### a."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2018-01-20T04:47:08.490000Z",
     "start_time": "2018-01-20T04:47:08.272000Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Text(0.5,1,u'Z distribution and Rejection region (shaded)')"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYsAAAEWCAYAAACXGLsWAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMS4wLCBo\ndHRwOi8vbWF0cGxvdGxpYi5vcmcvpW3flQAAIABJREFUeJzt3Xl8VNXZwPHfk4QQdgiELQkkYd9B\nAwoqbqi4AVZUtCq4lGprq7V9XaqvWrWt1bbWVqziiivg0r5YF9zAjcWEnQhICCEJayDsgYQkz/vH\nvbFDTDITyOTOTJ7v5zMfZu49985zJ5d55txz7jmiqhhjjDG1ifI6AGOMMaHPkoUxxhi/LFkYY4zx\ny5KFMcYYvyxZGGOM8cuShTHGGL8sWYQoEXlJRB52n58mIuvqcd8fiMhk9/kUEfmqHvf9YxH5qL72\nFywikisiY4K07wMikhYu+61vIpIlImcEad8/FZG/BVBuvojcWE/vecz/R3y3FZGmIrJWRDrWR1wN\nzZJFELhfmAeqeaiI3FfX/anql6raJ4D3fUBEXg1gf+er6oy6xlHN+6W4xxTjs+/XVPXc4923l9xE\nXer+zYpE5GMR6Rvo9qraUlVzjjOGH3zZ1cd+G4KqDlDV+fW9XxGJBe4FHqvvfTcEVS0BXgDu9DqW\nY2HJIgjcL8yWvg/gNmA78KxXcYnD/uaBedT9uyUCm4HnPY6nQfgm/hA0Hlirqpu9DuQ4vA5MFpGm\nXgdSV/bF0QBEZBjwODBJVbfWVEZElorIfhGZBcT5rDtDRAp8Xt8pIpvdsutE5GwRGQv8FrjC/UW8\nwi07X0R+LyJfA8VAWjW/WkVE/iEie91q8tk+K466XFOl9vKF++8e9z1HVq2yi8goEclw950hIqN8\n1s0XkYdE5Gv3WD4SkQ41fD7tROQ/IlIoIrvd50mB7ktErhGRTSKyS0Tuqe49qqOqh4DZwNAq8Vwv\nImvcWOaKSHefdSoiPd3nTUXkzyKSJyLbReRpEWnmU3a8iCwXkX0iskFExorI74HTgCfdz/XJavbb\nRkRedj+PTSJyb+UPgcq/gfu+u0Vko4icX9Mxun/jO0VkJXBQRGJEpKuIvO3uf6OI/NKnfDMRmeHu\ne42I3FHl/Pz+nHGP/28issV9/K3yi7LyvBaRX4vIDhHZKiLX1fLnOB/43Od94kTkVfdvusc9vzr5\nlO9ey/nwpohsc8/LL0RkgM+69iIyx/2bfAP0qPJ59RWntlkkzv+/ywPdVlULgN3AybUcZ0iyZBFk\nItIWeAt4uKaquTjV638DrwDxwJvApTWU7QPcAgxX1VbAeUCuqn4I/AGY5dZmhvhsdg0wFWgFbKpm\ntycBOUAH4H7gHRGJD+DwRrv/tnXfc2GVWOOB94C/A+2BvwLviUh7n2JXAdcBHYFY4Dc1vFcU8CLQ\nHegGHAKerFKm2n2JSH/gnzifQ1c3liQCICItgCuBbJ9lE3AS84+ABOBL4I0advEnoDdOsumJU1O5\nz93PCOBl4H+AtjifZ66q3uPu8xb3c72lmv3+A2gDpAGnA9e6x17pJGAdzt/0UeB5EZFaDvVK4EI3\njgrgXWCFG+/ZwG0icp5b9n4gxX3vc4Cra9nvPThfjEOBIcAInEtJlTq7x5EI3ABME5F2NexrkHtM\nlSa72ybj/E1vwjkvKtV2bn0A9HLXLQVe81k3DTgMdAGudx/A9+fDxzg1hI44n9tTPsmmxm19rHE/\ni/CiqvYI0gMQ4P/ch9RSbjSwxbcMsAAnwQCcARS4z3sCO4AxQJMq+3kAeLXKsvnAg9Usu9F9PqWa\n9/4GuMZ9nguMqe49cL4wFIjxWT8F+Mp9fg3wTZX3XghM8YnjXp91PwM+DPCzHQrsrnJM1e4L58t5\nps+6FkCp73FV2fdLOP/h9+B8cW4EBvus/wC4wed1FE6trbv7Wt2/kwAHgR4+ZUcCG93nzwCP1xDD\n938jn2WV+40GSoD+Put+Csz3+Rtk+6xr7m7buYb3ygWu93l9EpBXpczdwIvu8xzgPJ91N+Ken1XP\nGWADcIHPusofN+Cc14eqnD87gJNriHM9MNbn9fU4/08GV1M24HMLJ0EqTuKJBo4AfX3W/4H/ntNX\nAF9W2f4ZnARa67Y+y14D7gvkPA+lh9UsgutOYCAwWd2zpAZdgc1VylRXA0BVs3HaPx4AdojITBHp\n6ieOfD/rq3tvf/sMRFd+eBybcH5FVtrm87wYaFndjkSkuYg8415y2YdzCaytiEQHsK+u+HwGqnoQ\n2OUn9j+raluchHgI8O1g0B14wr30sQcowkkMiVX2kYDzRb3Ep+yH7nJwfhFv8BNHdTrg/FL2/Wxr\n/FxVtdh9Wu1n6/I9R7oDXStjduP+LVB5iadrlfK1nV9Vz4Gq59YuVS3zeV3jOYBz+aaVz+tXgLnA\nTPcS16Mi0sRnfbXng4hEi8gj7mW/fTjJDZzPNQGIqXJMvvF3B06q8tn8GKeG5G/bSq1wfoiEFUsW\nQSJO18F7gImq6u/E2AokVrlM0K2mwqr6uqqeinPiKs6lDtzn1W7i5/2re+8t7vODOF94lTrXYb9b\n3Bh9dcNpMK6rX+N8YZ+kqq357yWw2i6tVNqK88XsbCDSHOeyhV+qmgfcipMcKtsa8oGfqmpbn0cz\nVV1QZfOdOIlmgE+5Nuo0nFfupwfVq+2z3YnzC9b3sz3Wz7W698vHqf34Hl8rVb3AXb+Voy/jJVOz\nqueA77lVVytxLuk5AaseUdXfqWp/YBRwEc7lOH+uwmksH4NTm0hxlwtQCJRx9DH5/l/MBz6v8tm0\nVNWbA9i2Uj+cS3xhxZJFEIhIF2AmcJuqLgtgk4U4J9kv3cbFH+Fc261u331E5Cy3kfAwzpdRubt6\nO5Aide/x1NF97yYichnOyfy+u245MMldlw5M9NmuEOcyTU19/98HeovIVe5xXQH0B/5Tx/jA+TV2\nCKcxPR6n2h+ot4CLRORUt33oQepw7qvqxzhfcFPdRU8Dd1depxansfmyararwOn99ri4fetFJNHn\n2v/zwHXidFCIctdVdtHdTg2fq6qW4zS6/15EWonTuH474LfbdIC+AfaJ0+jdzP0lPlBEhrvrZ+Mc\nfzsRScRpQ6vJG8C9IpLgNjDfdxxxvo/TPgOAiJwpIoPc2uU+nARaXtPGPlrhXMbbhfND6A+VK9zP\n9h3gAbc22x+nbaTSf3DO6Wvc/xNNRGS4iPQLYFvczyseWFTno/eYJYvg+AlOlf0J+eG9Fk9XLayq\npTiNpVNwqtpX4Jx01WkKPILz63Ibzhf9b911b7r/7hKRpXWIdzFOY99O4Pc4taHKyzT/i/Prdzfw\nO5yGvcq4i93yX7tV8qN6eLj7uAinVrALuAO4SFV31iG2Sn8DmrkxLsK5nBMQVc0Cfu7GvtU9loJa\nN/qhx4A7RKSpqv4LpzY3072MsRqnp0517sRpHF/klv0E95KWqn6D0wD7OLAXp6dP5a/wJ4CJ4vQ4\n+ns1+/0FTq0vB/jKPbYX6nhM1XK/9C7GaRfaiPOZP4fzKxycZFvgrvsEJxmX1LC7h4FMnFrBKpzG\n5IePMbR3gb4+l107u++9D6fR+HMCS0Qv41we2gx8yw+/uG/BuWS1Daf96sXKFaq6HzgXmITzA2Ib\nzrnQ1N+2rquAGerccxFWpPZL6caYunBrdeU4jd15XsfTEETkZpxu4af7LXz87zUVp2H/tmC/V31z\nrwasAEar6g6v46krSxbG1CMRGYxTU2vj1hgjjnuZNQ3n8mkvnO7RT6qq32E4TPiyy1DG1BMRuRSY\nB9wZqYnCFYvTXXQ/8BlO1/CnPI3IBJ3VLIwxxvhlNQtjjDF+hfKgYXXSoUMHTUlJ8ToMY4wJK0uW\nLNmpqgn+ykVMskhJSSEzM9PrMIwxJqyISLWjRVRll6GMMcb4ZcnCGGOMX5YsjDHG+GXJwhhjjF+W\nLIwxxvgV1GQhzhSR60QkW0TuqqXcRHGmjEz3WXa3u906n1E6jTHGeCBoXWfdYYOn4Uy7WABkiMgc\nVf22SrlWwC9xxtOpXNYfZ1THATgTpXwiIr3d0TCNMcY0sGDeZzECZ2rHHAARmYkz4ci3Vco9hDNH\nsO/8uONxpsEsATaKSLa7v4UYEwYK95fw7dZ9bCw8wMHSclSVjq3jSO3QgkGJbYhrEu1/J8aEkGAm\ni0SOnl6wAGdu3++JyDAgWVX/IyK/qbLtoirbVp2ysnK44qkA3brVOLGcMQ1ib/ERZmbk8f6qrawo\n2FtjudiYKE7p0Z7L05MZ078TTaKt6dCEvmAmi+qmu/x+1EJ33P/HcSb8qdO23y9QnQ5MB0hPT7cR\nEY0n9h8+wpPzsnll4SaKS8sZktyW/zmvDyd0a0fPji1pFef8NyvcX8LabftZlLOL91Zu5ebXltIt\nvjm3jenFhKGJREUFMkOsMd4IZrIo4Oi5aJM4eu7dVsBAYL47/XNnYI6IjAtgW2NCwgertnLfnCx2\nHihh3JCu/HR0D/p3bV1t2eT45iTHN+ec/p347QX9+GztDv72yXfcPnsFry/O49GJg0lLaFnttsZ4\nLWhDlItIDPAdcDbO9IUZwFXuFJfVlZ8P/EZVM925jV/HaafoCnwK9KqtgTs9PV1tbCjTUA6VlvO7\nd7OYmZHPoMQ2PDRhIEOT29Z5PxUVyjvLNvPgu1mUlFXw0ISBXJ6e7H9DY+qJiCxR1XR/5YJWs1DV\nMhG5BZgLRAMvqGqWiDwIZKrqnFq2zRKR2TiN4WXAz60nlAkVhftLuHFGBis37+XmM3pw+zm9j7nd\nISpKmHhiEqN7deC2Wcu5462VrCrYy/0X9yfG2jJMCImYyY+sZmEaQvaOA0x58Rt2HSjliUlDOXdA\n53rbd1l5BY/OXcf0L3I4b0An/n7lMJrGWK8pE1yB1izsp4sxAcrecYBJ0xdx+EgFs356cr0mCoCY\n6Ch+e0E/7ruoP3OztnP9SxkcKrUKtQkNliyMCUBO4QGuetbpzT1z6skMTqp7+0Sgrj81lb9cNoQF\nG3Zx82tLKC2rCNp7GRMoSxbG+FG4v4RrX/iG8grl9Z+cRM+Owe+xdOmJSfzhkkHMX1fIr99cQXlF\nZFwuNuErYmbKMyYYDh8p5ycvZ7LzQAmzfzqS3p1aNdh7XzmiG3uKj/CnD9eS1K4Zd47t22DvbUxV\nliyMqUFFhfLr2StYUbCHf/74xKBeeqrJTaenkVdUzD/nb6Bv51aMH/qDgQyMaRB2GcqYGjz7ZQ7v\nrdrKnWP7MnZg/TZmB0pE+N24AQxPacedb69k9eaahxExJpgsWRhTjczcIh6du47zB3bmp6PTPI0l\nNiaKf159IvHNY7np1SXsO3zE03hM42TJwpgqig6Wcsvry0hq14w/TRyMOxyNpzq0bMo/rjqBrXsP\nc8+/VhMp90eZ8GHJwhgfqspdb6+k6GAp0646gdZxTbwO6Xsndm/Hr8b04t0VW3hrSYHX4ZhGxpKF\nMT7+tWwzH327nf85rw8DE9t4Hc4P3HxGT05Oi+f+OVnk7Sr2OhzTiFiyMMa1be9hHpiTRXr3dlx/\naqrX4VQrOkr46+VDiRbhrndW2uUo02AsWRiDe/npnZWUllfw2GVDiA7huSW6tm3G3Rf0Y8GGXczM\nyPe/gTH1wJKFMTiXn+avK+SusX1J7dDC63D8mjQ8mZPT4vnDe2vYtvew1+GYRsCShWn09hYf4Q/v\nr2FocluuHZnidTgBiYoS/nTpYI5UVHDvv1d7HY5pBCxZmEbvzx+to+hgKQ9PGBhWU5t2b9+C28/p\nzSdrtvPpmu1eh2MiXFCThYiMFZF1IpItIndVs/4mEVklIstF5CsR6e8uTxGRQ+7y5SLydDDjNI3X\nyoI9vLp4E9eOTAnJ3k/+XHdKKj07tuR3737L4SM2nLkJnqAlCxGJBqYB5wP9gSsrk4GP11V1kKoO\nBR4F/uqzboOqDnUfNwUrTtN4lVco9/57NR1aNuX2c3t7Hc4xaRIdxe/GDSCvqJhnv8jxOhwTwYJZ\nsxgBZKtqjqqWAjOB8b4FVHWfz8sWgPUDNA1mdmY+Kwv2cu+F/ULq5ru6OqVnBy4c1IVp87Mp2G33\nXpjgCGaySAR8+/UVuMuOIiI/F5ENODWLX/qsShWRZSLyuYicVt0biMhUEckUkczCwsL6jN1EuAMl\nZfzlo+9I796OcUO6eh3Ocfvthf0QhD++v9brUEyECmayqK6l8Ac1B1Wdpqo9gDuBe93FW4FuqjoM\nuB14XURaV7PtdFVNV9X0hISEegzdRLrpn29g54ES7rmwX0iM/XS8Ets2Y+roNN5btZVlebu9DsdE\noGAmiwIg2ed1ErCllvIzgQkAqlqiqrvc50uADUB4XlQ2IWfb3sNM/zKHi4d0ZVi3dl6HU29+MjqN\nDi2b8of319id3abeBTNZZAC9RCRVRGKBScAc3wIi0svn5YXAend5gttAjoikAb0Aa70z9eLPH62j\nogLuOK+P16HUq5ZNY7htTC8ycnfz8bfWldbUr6AlC1UtA24B5gJrgNmqmiUiD4rIOLfYLSKSJSLL\ncS43TXaXjwZWisgK4C3gJlUtClaspvH4dss+3l5awJRTUkiOb+51OPVu0vBk0hJa8MiHaykrr/A6\nHBNBJFKqq+np6ZqZmel1GCbE3fBSBpmbdvPFHWfSpln49oCqzUdZ25j6yhIenjCQq0/u7nU4JsSJ\nyBJVTfdXzu7gNo3G0rzdfLp2B1NHp0VsogA4p38n0ru34x+frbcb9Uy9sWRhGo2/fvQd7VvEMmVU\nitehBJWI8Otz+7B9XwmvL87zOhwTISxZmEZh4YZdfJW9k5vP6EGLpjFehxN0I3u0Z2Rae56av4FD\npVa7MMfPkoWJeKrKXz9eR6fWTRvVNfzbz+3NzgMlvLIo1+tQTASwZGEi3hfrd5KRu5tbzuxJXJNo\nr8NpMMNT4jmtVwee/jyHgyVlXodjwpwlCxPRnFrFdyS2bcblw5P9bxBhfnVOb4oOljJjYa7XoZgw\nZ8nCRLSvs3exIn8PPzuzB01jGk+totIJ3dpxZp8Epn+RwwGrXZjjYMnCRLQn562nU+umTDwxyetQ\nPHPrmN7sKT7C64s3eR2KCWOWLEzEyswtYlFOET85La1R1ioqDU1uy6k9O/DslxvtvgtzzCxZmIj1\n5Lxs4lvEctVJ3bwOxXM/O7MHhftLeGtJgdehmDBlycJEpNWb9zJ/XSE3nJpK89jIv6/Cn5Fp7RnW\nrS1Pf77Bxowyx8SShYlI0+Zl0youhmtGNp77KmojIvz8jJ4U7D7EuytrmynAmOpZsjARJ3vHfj7M\n2sbkkSlhPV1qfTurb0f6dm7FU/M2UFERGQOImoZjycJEnKfmbyAuJprrT031OpSQEhUl3HxGD9bv\nOMAna2y+C1M3lixMRNm69xBzlm/hiuHJxLeI9TqckHPhoC50b9+cafOybTY9UyeWLExEeWlBLhWq\n3GC1imrFREcxdXQaKwr2snijzSdmAhfUZCEiY0VknYhki8hd1ay/SURWichyEflKRPr7rLvb3W6d\niJwXzDhNZNh/+AivL8rjgkFdInIWvPpy6QlJtG8Ry3Nf2kzFJnBBSxbuHNrTgPOB/sCVvsnA9bqq\nDlLVocCjwF/dbfvjzNk9ABgLPFU5J7cxNZmVkc/+kjKmjk7zOpSQFtckmqtP7s4na3aQveOA1+GY\nMBHMmsUIIFtVc1S1FJgJjPctoKr7fF62ACovoo4HZqpqiapuBLLd/RlTrSPlFbz4dS4jUuMZnNTW\n63BC3jUju9M0Jornv9rodSgmTAQzWSQC+T6vC9xlRxGRn4vIBpyaxS/ruO1UEckUkczCwsJ6C9yE\nn/dXbWXznkNMPc1qFYHo0LIpPzohibeXFrDzQInX4ZgwEMxkIdUs+0H3C1Wdpqo9gDuBe+u47XRV\nTVfV9ISEhOMK1oQvVeXZL3NIS2jBWX07eh1O2Ljh1FRKyyp4ZaENMGj8C2ayKAB8JxBIAmq7dXQm\nMOEYtzWN2KKcIlZv3sdPTksjKqq63xmmOj07tuTsvh15ZdEmG2DQ+BXMZJEB9BKRVBGJxWmwnuNb\nQER6+by8EFjvPp8DTBKRpiKSCvQCvglirCaMPftlDh1axnLJsB9cqTR+3HhaGkUHS3l7qQ0waGoX\ntGShqmXALcBcYA0wW1WzRORBERnnFrtFRLJEZDlwOzDZ3TYLmA18C3wI/FxV7aeP+YH12/fz2dod\nXDsypVFNmVpfTk6LZ1BiG57/cqMNAWJqFdThOFX1feD9Ksvu83l+ay3b/h74ffCiM5Hgha83Etck\niqtPtgEDj4WIcONpqdw6czmfrt3BOf07eR2SCVF2B7cJW7sPlvKvZZu5ZFiiDe1xHC4Y1IWubeJ4\nwbrRmlpYsjBha1ZmPoePVDB5VIrXoYS1JtFRXDMyhYU5u1i7bZ//DUyjZMnChKWycqfL58i09vTt\n3NrrcMLelSOSiWsSxUtf53odiglRlixMWPpkzQ427zlktYp60ra505vsX8s2s/tgqdfhmBBkycKE\npRkLckls24wx/ewmvPoyeVQKJWUVvJGR53UoJgRZsjBhZ+22fSzM2cU1I7sTE22ncH3p27k1o3q0\n55WFm2yebvMD9j/NhJ0ZC3KJaxLFpOHJ/gubOpkyKoWtew8zN8tm0jNHs2RhwsqeYqe77IShibRt\nbt1l69vZ/TqRHN+MlxZYN1pzNEsWJqzMyrDussEUHSVMHplCRu5uVm/e63U4JoRYsjBho7xCeXnh\nJk5KjadfF+suGyyXpSfTPDaaF60brfFhycKEjU/WbGfznkNcd0qK16FEtDbNmnDpCUm8u2KLzXVh\nvmfJwoSNGQty6domjjH9bPyiYJs8KoXS8gpeX2zdaI3DkoUJC+u27WfBhl1cMzLFuss2gJ4dWzK6\ndwKvLtpEaZl1ozWWLEyYmLEwl6Yx1l22IV13Sgo79pfwweqtXodiQoAlCxPy9hYf4Z2lBUwYmkg7\nG122wZzeK4HUDi2sodsAlixMGJiVmWfdZT0QFSVMHtmd5fl7WJ6/x+twjMeCmixEZKyIrBORbBG5\nq5r1t4vItyKyUkQ+FZHuPuvKRWS5+5hTdVvTOFR2lx2RGk//rtZdtqFdemISLZvGMGNBrtehGI8F\nLVmISDQwDTgf6A9cKSL9qxRbBqSr6mDgLeBRn3WHVHWo+xiHaZQ+XbOdgt2HuM5qFZ5oFdeEiScm\n8Z+VW9ix/7DX4RgPBbNmMQLIVtUcVS0FZgLjfQuo6jxVLXZfLgKSghiPCUMzFjrdZW26T+9cO7I7\nR8qVNxbnex2K8VAwk0Ui4Ht2FbjLanID8IHP6zgRyRSRRSIyoboNRGSqWyazsLDw+CM2IeW77fv5\nOnsXV9vosp5KS2jJGX0SeHWxdaNtzIL5P1CqWabVFhS5GkgHHvNZ3E1V04GrgL+JSI8f7Ex1uqqm\nq2p6QkJCfcRsQsiMBbnExkQxaXg3r0Np9CaPSqHQutE2asFMFgWAb6f4JGBL1UIiMga4Bxinqt+P\nLaCqW9x/c4D5wLAgxmpCjNNddjMThnYl3rrLeq6yG+1L1tDdaAUzWWQAvUQkVURigUnAUb2aRGQY\n8AxOotjhs7ydiDR1n3cATgG+DWKsJsTMzszn0JFy6y4bIiq70S7Ls260jVXQkoWqlgG3AHOBNcBs\nVc0SkQdFpLJ302NAS+DNKl1k+wGZIrICmAc8oqqWLBqJ8grl5UW5jEiJZ0DXNl6HY1yXnphEi9ho\n60bbSMUEc+eq+j7wfpVl9/k8H1PDdguAQcGMzYSuz9buIL/oEHeN7ed1KMZHq7gmXJaezGuLN3H3\nBX3p2CrO65BMA7IuJibkzFiQS5c2cZw7wLrLhhrrRtt4WbIwIWX99v18lb2Tq0/uThPrLhty0hJa\ncnrvBF6zbrSNjv1vNCFlxsLK7rI2umyommKj0TZKlixMyNh76AhvL9nMuCFdad+yqdfhmBpYN9rG\nyZKFCRlvut1lp1h32ZAWFSVc63ajXWHdaBsNSxYmJFSOLjs8pR0DE627bKibaN1oGx1LFiYkzFu7\ng7yiYqaMSvU6FBOAym60767cQuH+Ev8bmLBnycKEhJesu2zY+b4b7Td5XodiGoAlC+M56y4bniq7\n0b66yLrRNgb2P9N47iV3dNkrR9josuFmyijrRttYWLIwnrLRZcPb6b0TSGnf3Bq6GwFLFsZTNrps\neIuKEiaPSmFp3h5WFlg32khW52QhIi3c+bWNOS7lFcqMhbmMSLXRZcNZZTdau0kvsvlNFiISJSJX\nich7IrIDWAtsFZEsEXlMRHoFP0wTiT5ds52C3Ye4zmoVYa1VXBMmnpjEf1ZstW60ESyQmsU8oAdw\nN9BZVZNVtSNwGrAIeMSdFtWYOpmxMJeubeI4p791lw13145KobS8wrrRRrBA5rMYo6pHqi5U1SLg\nbeBtEWlS75GZiPbd9v18nb2LO8f2Jca6y4a9HgktGe12o735jB7WBToC+f2LViYKEXleRIb6rhOR\nB3zLVCUiY0VknYhki8hd1ay/XUS+FZGVIvKpiHT3WTdZRNa7j8l1PC4T4l5akEtTG102olz3fTfa\nbV6HYoKgLun/POAlEbnWZ9m4mgq7jeDTgPOB/sCVItK/SrFlQLqqDgbeAh51t40H7gdOAkYA94tI\nuzrEakKY0122gEuGJdLOustGjMputC99vdHrUEwQ1CVZ7ABGA5eJyDQRiQGklvIjgGxVzVHVUmAm\nMN63gKrOU9Vi9+UiIMl9fh7wsaoWqepu4GNgbB1iNSFsVmYeh49UWHfZCOOMRmvdaCNVXZKFqOo+\nVb0YKAQ+B2rr75gI+M69WOAuq8kNwAd12VZEpopIpohkFhYWBnAIxmuVo8uenBZPvy6tvQ7H1LOJ\n6daNNlLVJVnMqXyiqg8AfwRyaylfXa1Dqy3o9KZKBx6ry7aqOl1V01U1PSEhoZZQTKj4KGsbBbsP\n2eiyEaq1TzfanQesG20kCeQ+CwFQ1ft9l6vqf1T1LN8yVRQAvq2XScCWavY/BrgHGKeqJXXZ1oSf\n577aSLf45tZdNoJ93412sXWjjSQB3WchIr8QkaNGeRORWBE5S0RmANX1VsoAeolIqojEApPwqZ24\n+xgGPIOTKHb4rJoLnCsi7dx8gEtzAAAbsklEQVSG7XPdZSaMLc3bzZJNu7n+lBSio2pr7jLh7Ptu\ntIs3caTcRqONFIEki7FAOfCGiGxxu7rmAOuBK4HHVfWlqhupahlwC86X/BpgtqpmiciDIlLZi+ox\noCXwpogsF5E57rZFwEM4CScDeNBdZsLY819upHVcDJelW3fZSHfdqBS277NutJFEVKttRqi+sHPz\nXQfgkKqGVHeH9PR0zczM9DoMU4P8omJOf2weU0f34K7z+3odjgmyigrl7L9+TutmTfj3z0ZR/ZVq\nEwpEZImqpvsrF0ibRZyI3CYiTwLXAYWhlihM6HtpQS5RIkwe1d1/YRP2oqKE609NZUX+HjJyd3sd\njqkHgVyGmoHTU2kVcAHwl6BGZCLOvsNHmJWRz0WDu9ClTTOvwzENZOIJSbRr3oTpX+R4HYqpB4Ek\ni/6qerWqPgNMxBlA0JiAzfomnwMlZdx4WprXoZgG1Cw2mmtGpvDJmu1sKDzgdTjmOAWSLL4f98lt\ntDYmYGXlFbz49UZOTotnYKLNWdHYXDuyO7ExUTz3pQ0BEu4CSRZDRGSf+9gPDK58LiL7gh2gCW8f\nrN7Glr2HufFUq1U0Rh1aNuXSE5J4e2mB3aQX5gIZdTZaVVu7j1aqGuPz3MZrMDVSVZ77MofUDi04\nq29Hr8MxHrnxtFSOlFfwsg0BEtZs0HkTNBm5u1lRsJfrT00lym7Ca7R6JLRkTL9OvLxoE4dKy70O\nxxwjSxYmaJ7+fAPxLWKZeEKS/8Imok0dncae4iO8tSTff2ETkixZmKBYs3Ufn63dwXWjUmgWG+11\nOMZj6d3bMTS5Lc99tZHyisBvBDahw5KFCYpnPt9Ai9horh2Z4nUoJgSICFNHp7FpVzEff2tDgIQj\nSxam3uUXFfPuyq1cdVI32jS36dmN47wBnekW35ynP8+hLsMMmdBgycLUu+e+zCFK4AbrLmt8REcJ\nPxmdxvL8PSzcsMvrcEwdWbIw9WrngRJmZuRzybBEOreJ8zocE2IuOzGJhFZNeXJettehmDqyZGHq\n1YwFuZSWVzB1dA+vQzEhKK5JND85LZUFG3axNM8GGAwnlixMvTlQUsaMBbmc178zPTu29DocE6J+\nfFJ32jRrwlNWuwgrQU0WIjJWRNaJSLaI3FXN+tEislREykRkYpV15e6ESN9PimRC2xuL89h3uIyb\nzrBahalZi6YxXHdKCp+s2cGarTZiULgIWrIQkWhgGnA+0B+4UkT6VymWB0wBXq9mF4dUdaj7GFfN\nehNCDh8p59kvcxiZ1p6hyW29DseEuCmjUmgRG81T8zd4HYoJUDBrFiOAbFXNUdVSYCYw3reAquaq\n6krAJuoNc7My8tmxv4RfnN3T61BMGGjbPJarT+7Oeyu3sHHnQa/DMQEIZrJIBHzv7S9wlwUqTkQy\nRWSRiEyo39BMfSopK+ef8zcwIiWekWntvQ7HhIkbTkslJjqKp612ERaCmSyqGzmuLnfidHPnhb0K\n+JuI/OBCuIhMdRNKZmFh4bHGaY7T7Ix8tu07zK1jetlcyyZgHVvFcUV6Mu8sK2DLnkNeh2P8CGay\nKACSfV4nAVsC3VhVt7j/5gDzgWHVlJmuqumqmp6QkHB80ZpjUlJWzlPzN5DevR2jelitwtTNT09P\nQxWemm89o0JdMJNFBtBLRFJFJBaYBATUq0lE2olIU/d5B+AU4NugRWqO2ZuZBWzda7UKc2yS2jXn\n8uHJzMrIp2B3sdfhmFoELVm4U7DeAswF1gCzVTVLRB4UkXEAIjJcRAqAy4BnRCTL3bwfkCkiK4B5\nwCOqaskixJSWVfDP+RsY1q0tp/bs4HU4JkzdcmZPBGGa3XcR0mKCuXNVfR94v8qy+3yeZ+Bcnqq6\n3QJgUDBjM8fvrSUFbN5ziN9fMtBqFeaYdW3bjCtHJPPa4jxuPr0n3do39zokUw27g9sck9KyCqbN\ny2ZIcltO723tReb4/OzMnkRFCX//bL3XoZgaWLIwx2RWRh6b9xziNmurMPWgU+s4rj6pO+8sLbD7\nLkKUJQtTZ8WlZfz9s2xGpMRzhtUqTD25+YwexMZE8fdPrXYRiixZmDp7aUEuhftLuGNsH6tVmHqT\n0Kopk0em8H/LN5O9Y7/X4ZgqLFmYOtlbfISn52/grL4dSU+J9zocE2Gmjk4jrkk0f/34O69DMVVY\nsjB18vQXG9h3uIzfnNvH61BMBGrfsik3npbG+6u2sczmuwgplixMwHbsO8yLX29k/NCu9O/a2utw\nTISaOjqNDi1j+eMHa22u7hBiycIE7B+fZVNWrtx+Tm+vQzERrGXTGG49uxffbCzis7U7vA7HuCxZ\nmIBs3HmQN77J44rhyXRv38LrcEyEmzSiG6kdWvDIB2spK7cZDEKBJQsTkD++v4amMVHcOqaX16GY\nRqBJdBR3nNeH9TsO8PbSAq/DMViyMAFYuGEXH327nZ+d2ZOOreK8Dsc0EmMHdmZYt7b89ePvOFRa\n7nU4jZ4lC1Or8grl4fe+pWubOG44NdXrcEwjIiLcfX4/tu8r4bkvc7wOp9GzZGFq9c7SArK27OPO\n8/sS1yTa63BMIzMiNZ7zBnTiqfkb2LrXJkjykiULU6Pi0jIem7uOocltGTekq9fhmEbq3gv7U67K\nIx+s9TqURs2ShanR05/nsGN/Cf97UT8b1sN4Jjm+OT8dncb/Ld9CZm6R1+E0WpYsTLU27TrI059v\n4KLBXTixuw3rYbx18xk96NImjvvnZFFeYTfqeSGoyUJExorIOhHJFpG7qlk/WkSWikiZiEyssm6y\niKx3H5ODGac5mqrywJwsmkQJ917Y3+twjKF5bAx3X9CPrC37mJ2Z73U4jVLQkoWIRAPTgPOB/sCV\nIlL1mycPmAK8XmXbeOB+4CRgBHC/iLQLVqzmaHOztjNvXSG/Oqc3ndtYV1kTGi4e3IURKfE8Nncd\ne4uPeB1OoxPMmsUIIFtVc1S1FJgJjPctoKq5qroSqHqL5nnAx6papKq7gY+BsUGM1biKS8t48N0s\n+nRqxeRRKV6HY8z3RIQHxg1g76EjPPKhNXY3tGAmi0TAt75Y4C6rt21FZKqIZIpIZmFh4TEHav7r\nH59ls2XvYR6aMJAm0dakZUJL/66tuf6UFN74Jo8Ma+xuUMH8Nqiu+0ygLVMBbauq01U1XVXTExJs\nxrbjlb1jP899mcOPTkhkRKo1apvQ9KtzepPYthl3v7OKkjK7s7uhBDNZFADJPq+TgC0NsK05BuUV\nyp1vr3IaEs/v53U4xtSoeWwMD18ykOwdB3jmc7uzu6EEM1lkAL1EJFVEYoFJwJwAt50LnCsi7dyG\n7XPdZSZIXl6Yy5JNu7nvov4ktGrqdTjG1OrMPh25aHAXnpyXTU7hAa/DaRSClixUtQy4BedLfg0w\nW1WzRORBERkHICLDRaQAuAx4RkSy3G2LgIdwEk4G8KC7zARBflExj364jtN7J/CjEwJtVjLGW/dd\n3J+4mCjufmcVFXbvRdBJpMxElZ6erpmZmV6HEXZUlaufX8zyvD18dPvpJLZt5nVIxgRsVkYed769\nigcu7s+UU2ygy2MhIktUNd1fOevu0sjNysjn6+xd3H1BP0sUJuxcnp7MWX078siHa+1yVJBZsmjE\n8nYV89B/vuXktHiuGtHN63CMqTMR4ZEfDaJpTDS/fnOFzaoXRJYsGqmy8gpum7WMqCjhL5cPJSrK\nBgo04alj6zgemjCQZXl7mG7zXgSNJYtGatq8DSzN28PDEwba5ScT9i4e3IULB3Xh8Y+/I2vLXq/D\niUiWLBqhpXm7+ftn6xk/tCvjh1rvJxP+RISHJgwkvkUsv3h9GQdLyrwOKeJYsmhk9h8+wq9mLadz\n6zgeHD/Q63CMqTfxLWJ5YtIwcncd5H//vdrrcCKOJYtGRFW5462VFOw+xONXDKVNsyZeh2RMvTo5\nrT2/PLsX7yzbzFtLCrwOJ6JYsmhEXvg6lw9Wb+OO8/rY2E8mYv3irF6cnBbP//57Ndk7rDttfbFk\n0Uhk5hbxx/fXcE7/TkwdneZ1OMYETXSU8MSkYTSLjeamV5ew/7DNfVEfLFk0AjsPlHDL68vo2rYZ\nf75siM2nbSJep9ZxPHnlMDbuPMjts1fYcCD1wJJFhCspK+emV5awu7iUp358grVTmEZjVM8O3HNB\nPz7+djt//2y91+GEPUsWEUxVufudVWRu2s2fLxvCwMQ2XodkTIO67pQULj0hib99sp6PsrZ5HU5Y\ns2QRwZ7+PId3lm7mtjG9uHhIV6/DMabBiQi/v2QgQ5La8KtZy1m92W7YO1aWLCLU3KxtPDp3LRcP\n6cqtZ/fyOhxjPBPXJJrp16bTplkTrnspg/yiYq9DCkuWLCJQRm4Rv3xjGUOS2vLYxMHWoG0avU6t\n43jp+hGUHClnyovfsKe41OuQwo4liwizZus+rn8pg8R2zXh+cjpxTaK9DsmYkNC7UyumX5tOftEh\nbpyRyeEjNn93XQQ1WYjIWBFZJyLZInJXNeubisgsd/1iEUlxl6eIyCERWe4+ng5mnJEib1cx177w\nDS1iY3jlhpNo39KmRzXG18lp7fnL5UNYkrebm15dQkmZJYxABS1ZiEg0MA04H+gPXCki/asUuwHY\nrao9gceBP/ms26CqQ93HTcGKM1IU7C7mx88v4kh5Ba/cMMJGkjWmBhcP6cofLhnE/HWF/Py1ZZSW\n2RwYgQhmzWIEkK2qOapaCswExlcpMx6Y4T5/Czhb7AJ7neUXFTNp+iL2FB9hxnUj6NWpldchGRPS\nrhzRjQfHD+CTNdu5deYymzQpAMFMFolAvs/rAndZtWVUtQzYC7R316WKyDIR+VxETqvuDURkqohk\nikhmYWFh/UYfJvKLirn8mYUUHSzltRtPYkhyW69DMiYsXDsyhXsv7McHq7dx68zlVsPwIyaI+66u\nhlD1nvuaymwFuqnqLhE5Efi3iAxQ1X1HFVSdDkwHSE9Pb3T386/fvp+rn19M4f4Sbjw1lcFJliiM\nqYsbT3PGSXv4vTXsO3yEZ645keaxwfxaDF/BrFkUAMk+r5OALTWVEZEYoA1QpKolqroLQFWXABuA\n3kGMNexk5hbxo38uoHB/CRXqTC1pjKm7G09L49FLB/N19k5+/Nxi61Zbg2Amiwygl4ikikgsMAmY\nU6XMHGCy+3wi8JmqqogkuA3kiEga0AuwyXVdH3+7naueXczBknIqx0er0EZXsTKm3lw+PJmnfnwC\nWZv3cfkzC+3GvWoELVm4bRC3AHOBNcBsVc0SkQdFZJxb7HmgvYhkA7cDld1rRwMrRWQFTsP3Tapa\nFKxYw4Wq8tyXOUx9JZOyioqjEkSZjappzHEZO7ALL103nK17DzNh2tdk5Db6r5yjiEbIL9L09HTN\nzMz0OoygOXyknLveXsm/l1e9kuf4zbm9ueUsG9bDmOO1ofAAN87IpGB3MX+4ZBCXpSf73yiMicgS\nVU33V87u4A4Dm/cc4tJ/LqgxUQBYzz9j6kePhJb8+2encFJqe/7nrZXc++9Vdrc3lixC3nsrt3Le\n41+wZuu+WsuVR0gN0ZhQ0KZ5E168bjhTR6fx6qI8LnlqARsKG/cUrZYsQtTBkjLueGsFP399KcWl\nZfhrkrCZwIypX02io/jtBf14ccpwtu09xMX/+IrZGflEyqX7urJkEYIWbNjJ2Ce+YHZmAYDfRAFW\nszAmWM7s25EPbh3NoMQ23PH2Sqa8mMGWPYe8DqvBWbIIIXuKS/mfN1dw1bOL2by7biej1SyMCZ7O\nbeJ44ycn87txA8jILeLcx7/gtcWbGtX/O0sWIaC8QpmVkceZf57PW0sCr034sq6zxgRXVJQweVQK\nc28bzeCkNtzzr9WMn/Y1SzY1ji62liw89tX6nVzwxBfc+fYq9hQf+cF4KIEqt2RhTINIjm/Oazee\nxBOThlK4v4RL/7mQ22Yuo2B3ZN/IZ4OgeCQjt4gnPlnPV9k7iXJHyDqer3u7g9uYhiMijB+ayJh+\nnXj68w0880UO763ayuXpyfz8zJ50jcApAixZNCBVZeGGXTzx6XoWbyz6PknUR6XAahbGNLwWTWP4\n9bl9uOqkbkybl82sjHzezCzgiuHJ3HBqKikdWngdYr2xZNEADpWW83/LN/PiglzWbdtfr0miktUs\njPFOlzbNeHjCIG46vQfT5mUzMyOPVxdv4uy+nbj+1BRGprUn3KfqsWQRJKpK1pZ9vLN0M7Mz8zlQ\nUhaUJFHJahbGeC+pXXP++KPB/GpMb15dtIlXF+fxybPb6dWxJZeemMQlwxLpFKYjRFuyqGf5RcXM\nWbGFd5YWsKHwIMJ/2yKC+X1uvaGMCR0dW8dx+7l9+NmZPZmzfAuzMvN55IO1PPrhWk7rlcCEYV05\nq08n2jRv4nWoAbNkcZzKK5Tl+Xv4dM12Plmzne+2O0MC1EejdV00pv7exoSLuCbRXD48mcuHJ5NT\neIB3lm7mnaUF/GrWCmKihJPT2nPugE6c2acjyfHNvQ63VpYs6qi8Qlm7bR/fbCxicc4uFuYUsffQ\nEQB8L0k29Hd3ueUKY0JaWkJLfnNeH24/pzcrCvYwN2s7H2Vt477/ywKySI5vxik9OjCqZwdOTo0P\nuQnNLFnUQlXJLzrE6i17Wb15L1lb9rFk024OlJQBEB0lR7UVeNnGbDULY8JDVJQwrFs7hnVrx13n\n9yV7xwG+XF/Igg27eG/VVmZm5AOQ2LYZQ5LbMCSpLUOS29Kvc2tPL1tZsgBKysrZtKuYnMKD5Ow8\nwMbCg2woPMC67fs5WOIMTSw4NQff7+RQalQOpViMMYHr2bElPTu25LpTUikrr2D1ln1k5haxomAv\nK/L38P6qbd+X7diqKb07taJnx5b07tSK1A4t6N6+OZ1bxxEVFdzeVkFNFiIyFngCiAaeU9VHqqxv\nCrwMnAjsAq5Q1Vx33d3ADUA58EtVnRuMGLfsOcSpf/rsqCQQHSVUVOhR7Q2KtzUHf2wgQWPCX0x0\nFEOT2zI0ue33y4oOlrKiYA/fbdvP+h0HWL99P7Mz8yku/e8cG4MS2/DuL04NbmzB2rE7h/Y04Byg\nAMgQkTmq+q1PsRuA3araU0QmAX8CrhCR/jhzdg8AugKfiEhvVa33GUg6tY5j7IDOvL/6v9k7HH+l\n22UoYyJTfItYzuzTkTP7dPx+WUWFsmXvIXJ3FrOp6CBNY6KDHkcwaxYjgGxVzQEQkZnAeMA3WYwH\nHnCfvwU8Kc6dK+OBmapaAmx05+geASys7yCjo4QJwxJZtDG8BwNrFhv8k8UYExqiooSkds1Jatec\nU+nQIO8ZzGSRCOT7vC4ATqqpjKqWicheoL27fFGVbROrvoGITAWmAnTr1u2YAz13QGfOHdD5mLc3\nxphIF8xRZ6trbal6raSmMoFsi6pOV9V0VU1PSEg4hhCNMcYEIpjJogBI9nmdBGypqYyIxABtgKIA\ntzXGGNNAgpksMoBeIpIqIrE4DdZzqpSZA0x2n08EPlNngts5wCQRaSoiqUAv4JsgxmqMMaYWQWuz\ncNsgbgHm4nSdfUFVs0TkQSBTVecAzwOvuA3YRTgJBbfcbJzG8DLg58HoCWWMMSYwohHSPz89PV0z\nMzO9DsMYY8KKiCxR1XR/5WxaVWOMMX5ZsjDGGOOXJQtjjDF+WbIwxhjjV8Q0cItIIbDpOHbRAdhZ\nT+F4KVKOA+xYQlWkHEukHAcc37F0V1W/dzVHTLI4XiKSGUiPgFAXKccBdiyhKlKOJVKOAxrmWOwy\nlDHGGL8sWRhjjPHLksV/Tfc6gHoSKccBdiyhKlKOJVKOAxrgWKzNwhhjjF9WszDGGOOXJQtjjDF+\nWbJwichDIrJSRJaLyEci0tXrmI6ViDwmImvd4/mXiLT1v1VoEpHLRCRLRCpEJOy6OYrIWBFZJyLZ\nInKX1/EcDxF5QUR2iMhqr2M5HiKSLCLzRGSNe27d6nVMx0pE4kTkGxFZ4R7L74L2XtZm4RCR1qq6\nz33+S6C/qt7kcVjHRETOxZkbpExE/gSgqnd6HNYxEZF+QAXwDPAbVQ2boYVFJBr4DjgHZ0KvDOBK\nVf221g1DlIiMBg4AL6vqQK/jOVYi0gXooqpLRaQVsASYEI5/FxERoIWqHhCRJsBXwK2qusjPpnVm\nNQtXZaJwtaCaaVzDhap+pKpl7stFODMNhiVVXaOq67yO4xiNALJVNUdVS4GZwHiPYzpmqvoFzrwz\nYU1Vt6rqUvf5fmANkOhtVMdGHQfcl03cR1C+uyxZ+BCR34tIPvBj4D6v46kn1wMfeB1EI5UI5Pu8\nLiBMv5QilYikAMOAxd5GcuxEJFpElgM7gI9VNSjH0qiShYh8IiKrq3mMB1DVe1Q1GXgNuMXbaGvn\n71jcMvfgzDT4mneR+hfIsYQpqWZZ2NZYI42ItATeBm6rcmUhrKhquaoOxbmCMEJEgnKJMGjTqoYi\nVR0TYNHXgfeA+4MYznHxdywiMhm4CDhbQ7xhqg5/l3BTACT7vE4CtngUi/HhXt9/G3hNVd/xOp76\noKp7RGQ+MBao904IjapmURsR6eXzchyw1qtYjpeIjAXuBMaparHX8TRiGUAvEUkVkVicOebneBxT\no+c2Cj8PrFHVv3odz/EQkYTK3o4i0gwYQ5C+u6w3lEtE3gb64PS82QTcpKqbvY3q2IhINtAU2OUu\nWhTGPbsuAf4BJAB7gOWqep63UQVORC4A/gZEAy+o6u89DumYicgbwBk4w2FvB+5X1ec9DeoYiMip\nwJfAKpz/7wC/VdX3vYvq2IjIYGAGzvkVBcxW1QeD8l6WLIwxxvhjl6GMMcb4ZcnCGGOMX5YsjDHG\n+GXJwhhjjF+WLIwxxvhlycKYGojIJe4oxL6PChE5vx72fcB/KWNCh3WdNSZAIjIVZ9ywM1W1wl95\nP/s6oKot6ycyY4LPahbGBEBEeuMMLnlN1UQhIn8SkZ/5vH5ARH4tIi1F5FMRWSoiq6ob60pEzhCR\n//i8flJEprjPTxSRz0VkiYjMdYfWNsYTliyM8cMdR+h1nPk08qopMhO4wuf15cCbwGHgElU9ATgT\n+Is71ESg7/kPYKKqngi8AITt3d8m/DWqgQSNOUYPAVmqOrO6laq6TEQ6urMrJgC7VTXP/cL/gztp\nUAXO8OSdgG0BvGcfYCDwsZtfooGtx38oxhwbSxbG1EJEzgAuBU7wU/QtYCLQGaemAU77RgJwoqoe\nEZFcIK7KdmUcXcOvXC84CWrkMQdvTD2yy1DG1EBE2gEvAte6M6rVZibOqLITcRIHQBtgh5sozgS6\nV7PdJqC/iDQVkTbA2e7ydUCCiIx0Y2kiIgOO74iMOXZWszCmZjcBHYF/Vmlq+KOqzvJdoKpZ7nzO\nm1W18nLRa8C7IpIJLKeaoaNVNV9EZgMrgfXAMnd5qYhMBP7uJpEYnNFrs+rzAI0JlHWdNcYY45dd\nhjLGGOOXJQtjjDF+WbIwxhjjlyULY4wxflmyMMYY45clC2OMMX5ZsjDGGOPX/wM1z3IgbH+njgAA\nAABJRU5ErkJggg==\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0xc15c048>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "from scipy.stats import binom, norm\n",
    "from math import sqrt\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "p = 0.5 # null hypothesis\n",
    "n = 12\n",
    "\n",
    "mu_0 = n * p\n",
    "sigma_0 = sqrt(n * p * (1 - p))\n",
    "sigma_X = sigma_0 / sqrt(n)\n",
    "\n",
    "alpha = norm.ppf(0.05)\n",
    "\n",
    "# Central Limit Theorem\n",
    "\n",
    "x = np.linspace(-3, 3, 1000)\n",
    "Z = norm.pdf(x)\n",
    "\n",
    "alpha_plot = norm.pdf(x)\n",
    "\n",
    "alpha_plot[alpha_plot > norm.cdf(alpha)] = 0\n",
    "alpha_plot[len(alpha_plot)/2:] = 0\n",
    "\n",
    "z_value = (3 - mu_0)/sigma_X\n",
    "p_value = norm.cdf(z_value)\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(x, Z)\n",
    "plt.fill_between(x, alpha_plot)\n",
    "plt.xlabel(\"Z value\")\n",
    "plt.ylabel(\"P(z)\")\n",
    "plt.title(\"Z distribution and Rejection region (shaded)\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2018-01-20T04:47:39.762000Z",
     "start_time": "2018-01-20T04:47:39.749000Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "9.8658764503769458e-10"
      ]
     },
     "execution_count": 38,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "p_value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2018-01-20T04:26:11.133000Z",
     "start_time": "2018-01-20T04:26:11.126000Z"
    }
   },
   "source": [
    "Ericka should conclude that the coin is biased toward tails."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2018-01-20T04:10:40.699000Z",
     "start_time": "2018-01-20T04:10:40.692000Z"
    }
   },
   "source": [
    "#### b.\n",
    "\n",
    "Strictly speaking, Jon _can_ still compute a $p$-value.  Some might argue that the result will only be significant if the bias is strong.  There are a lot of things that can throw off the result with a small $n$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### c.\n",
    "\n",
    "I ran out of time (sorry).  I've got to get this downloaded and pushed, before the midnight deadline."
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.14"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
