git_url:	https://jeremysamuelson@bitbucket.org/jeremysamuelson/data_science.git

issues:		None.  This was easy.  The "hardest" part was looking up a stopwords corpus, since we could only use re.

results:	The code outputs a list containing only the bullet points from the resume, with all non alphanumeric
		characters removed.
