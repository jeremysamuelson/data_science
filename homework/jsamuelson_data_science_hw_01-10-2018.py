
def knight_moves(cell):
    rank = int(cell[1])
    file = ord(cell[0]) - (65 + 32) + 1
    
    n_moves = 8

    if rank <= 2 or (9 - rank) <= 2:
        n_moves -= 2
    
        if file <= 2 or (9 - file) <= 2:
            n_moves -= 2
            
            if rank + file == 3 or (9 - rank) + file == 3 or rank + (9 - file) == 3 or (9 - rank) + (9 - file) == 3:
                n_moves -= 1
            
            elif rank in {1,8} or file in {1,8}:
                n_moves -= 2
            
            else:
                pass

    elif file <= 2 or (9 - file) <= 2:
        n_moves -= 2
    
        if file in {1,8}:
            n_moves -= 2
        
    else:
        pass

    return n_moves
            
    

# Test cases

cases = ["a1", "a2", "c2", "d4", "g6", "h7"]

for case in cases:
    print case + " --> " + str(knight_moves(case)) + " moves" 

