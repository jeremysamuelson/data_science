repository url: 	https://jeremysamuelson@bitbucket.org/jeremysamuelson/data_science.git

project overview: 	Building a classification model to detect whether questions posted on quora.com are semantic duplicates.

thought process: 	Because time is short, I've decided to run with a logistic regression model.  It's a relatively simple,
			but powerful model, especially for binary classifications (i.e. "duplicate", "not duplicate").  In addition
			to classifying question pairs as duplicates or non-duplicates, I'd also like to use the model to gain an
			understanding as to different topics appearing in questions on quora.com.  I believe being able to show a
			high level of interest in particular topics on quora.com may be used to justify higher prices on ad space for
			companies in particular categories, more interest in ad space from these companies, or both.

results:		A logistic regression model was fitted to the data using sklearn, and optimized using stochastic gradient
			descent.  A second logistic regression model was implemented from scratch.  The transformation on the questions
			was also implemented from scratch, and is derived from the Jaccard Similarity Coefficient.  This second
			model is currently unoptimized due to lack of time.

issues:			Word2Vec proved extremely difficult to implement, which is why the second method using the Jaccard Similarity
			Coefficient was implemented.  I was able to get code to run without errors using the gensim module, but I
			couldn't figure out through documentation or tinkering how to get the word2vec object to return vectorized
			versions of the questions.  Another issue with word2vec is that, judging from the documentation, this is
			actually an entirely separate model which requires a completely separate training and optimization before a
			desirable input for the logistic regression model can be created.  I wasn't able to figure this out, but
			given more time and research, I'm sure this can be done

improvements:		This week a version of the model was regularized with the LASSO method and optimized with stochastic gradient
			descent.  A second version of the logistic regression model was also implemented from scratch.

next steps:		Implementation of word2vec to optimize model inputs.


 