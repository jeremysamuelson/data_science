#For the final project as part of your technical evaluatation you must get the
	#code below to compile and pass all of the below test cases below
	#without using imports such as numpy. You are only allowed imports in the
	#math module from python 3's standard library.

#You can also not under any circumstance modify the core idea of the algorithm.
	#Which is starting with an n by n zero matrix and creating the spiral pattern by
	#setting the outer most rows and cols, then working your way inward into the matrix
	#until the spiral pattern is achieved.

#So don't do the following ->
	#try to develop a mathimatical solution, or even try to use a while loop / recursion to replace a for loop,
	#although you can certainly modify the range parameters, variables, how a variable is calculated...
	#You can't delete entire functions.
	#Have additional space complexity exceeding O(1) per function
	#Delete entire lines
#A good rule of thumb is if your changes are the lowest number of character modifications to 
	#get the code to pass the test cases, then you can certainly make those changes.

#This is a deliverable that will be utilized to judge your technical skill in addition to your final project.

#At the end of the day if something is unclear, it is Your Responsibility to Seek Clarity
	#and Seek Out Help on completing this assignment. I will not be pressuring you to complete this assignment, 
	#since everyone here is an adult and will in short order be a data scientist, so I will treat you accordingly.

testCase = [[ 1, 2, 3, 4, 5],
            [16,17,18,19, 6],
            [15,24,25,20, 7],
            [14,23,22,21, 8],
            [13,12,11,10, 9]]

testCase2 = [[1, 2, 3],
             [8, 9, 4],
             [7, 6, 5]]

testCase3 = [[ 1, 2, 3, 4, 5, 6],
             [20,21,22,23,24, 7],
             [19,32,33,34,25, 8],
             [18,31,36,35,26, 9],
             [17,30,29,28,27,10],
             [16,15,14,13,12,11]]

testCase4 = [[1,2,3,4,5,6,7,8,9,10], 
 			 [36,37,38,39,40,41,42,43,44,11], 
 			 [35,64,65,66,67,68,69,70,45,12], 
 			 [34,63,84,85,86,87,88,71,46,13], 
 			 [33,62,83,96,97,98,89,72,47,14], 
 			 [32,61,82,95,100,99,90,73,48,15], 
 			 [31,60,81,94,93,92,91,74,49,16], 
 			 [30,59,80,79,78,77,76,75,50,17], 
 			 [29,58,57,56,55,54,53,52,51,18], 
 			 [28,27,26,25,24,23,22,21,20,19]]

testCase5 = [[1]]

def setCol(matrix, colN, listOfValues, iter = 0, modIndexZero = True):
	if iter > 0:
		rangeStart = iter
		rangeEnd = len(matrix)-iter
	else:
		rangeStart = 0
		rangeEnd = len(matrix)
		
	list_row = 0
	
	for row in range(rangeStart,rangeEnd):
		if modIndexZero:
			matrix[row][colN] = listOfValues[list_row]
		else:
			if row > 0:
				matrix[row][colN] = listOfValues[list_row]
		list_row += 1

def setRow(matrix, rowN, listOfValues, iter = 0):
	if iter > 0:
		rangeStart = iter
		rangeEnd = len(matrix[0])-iter
	else:
		rangeStart = 0
		rangeEnd = len(matrix[0])
	matrix[rowN][rangeStart:rangeEnd] = listOfValues

def getListOfValues(n, iter = 0, start = 1):
	n -= iter*2
	out = []
	if n == 1:
		return {"matrix":[[start]],"start":start}

	for i in range(4):
		out.append(list(range((0+n*i-1*i+1)+start-1, (n+n*i-1*i+1)+start-1)))
	if start == 0:
		start = 1
		start += 4*(n-1)
	else:
		start += 4*(n-1)
	return {"matrix":out, "start":start}

def spiralNumbers(n):
	nbyn = n*n
	matrix = [[0 for _ in range(n)] for _ in range(n)]
	left = 0
	right = n-1
	bottom = 0
	top = n-1
	starter = 1
	from math import ceil
	for j in range(ceil(len(matrix)/2)):
		valueDict = getListOfValues(n, iter = j, start = starter)
		if j == ceil(len(matrix)/2)-1 and len(matrix)%2==1:
			matrix[int(len(matrix)/2)][int(len(matrix)/2)] = int(valueDict["start"])
		else:
			setCol(matrix, right, valueDict["matrix"][1], iter = j)
			setCol(matrix, left, list(reversed(valueDict["matrix"][3])), iter = j)
			setRow(matrix, top, list(reversed(valueDict["matrix"][2])), iter = j)
			setRow(matrix, bottom, valueDict["matrix"][0], iter = j)
		starter = valueDict["start"]
		left += 1
		right -= 1
		bottom += 1
		top -= 1

	return matrix

if __name__=='__main__':
	print("Output ->")
	print("\nLets see if we pass our test cases ->")
	print("test case 1: -> {}".format(spiralNumbers(5) == testCase))
	print("test case 2: -> {}".format(spiralNumbers(3) == testCase2))
	print("test case 3: -> {}".format(spiralNumbers(6) == testCase3))
	print("test case 4: -> {}".format(spiralNumbers(10) == testCase4))
	print("test case 5: -> {}".format(spiralNumbers(1) == testCase5))
