import pandas as pd


class Network(object):
    
    """
    A class for structuring data into networks.
    Inputs:
        nodes: A list or set of unique node names (converted to set upon initialization)
        edges: A list of tuples containing node pairs to indicate network connections.
               If edges are to be directed, then tuples are to be ordered in a (from, to)
               format.
    Methods:
        get_node_weights(): Returns a two-column Pandas DataFrame object with one column giving the
        node names and the other giving the weight.
            
            Arguments:
                direction: Either "to" or "from". Determines how weight is calculated in directed
                networks
                
        get_edge_weights(): Returns a three-column Pandas DataFrame object with the first column
        giving the "from" nodes, the second giving the "to" nodes, and the third giving the weights
    """
    
    def __init__(self, nodes, edges):
        self.nodes = set(nodes)
        self.edges = edges
        
        
    def __str__(self):
        return "Nodes: {n}\nEdges: {e}".format(n=len(self.nodes), e=len(self.edges))
    
    def __len__(self):
        return len(self.nodes)
    
    def get_node_weights(self, direction="to"):
        if direction == "to":
            idx = 1
        else:
            idx = 0
            
        nodes = [edge[idx] for edge in self.edges]        
        node_dict = {}
        
        for i in nodes:
            if i in node_dict:
                node_dict[i] += 1
            else:
                node_dict[i] = 1
        
        return pd.DataFrame({"nodes":node_dict.keys(), "node_weights":node_dict.values()})
    
    def get_edge_weights(self):
        edge_dict = {}
        
        for i in self.edges:
            if i in edge_dict:
                edge_dict[i] += 1
            else:
                edge_dict[i] = 1
        
        return pd.DataFrame({ \
            "from_nodes":[i[0] for i in edge_dict.keys()], \
            "to_nodes":[i[1] for i in edge_dict.keys()], \
            "edge_weights": edge_dict.values() \
            })
                          

