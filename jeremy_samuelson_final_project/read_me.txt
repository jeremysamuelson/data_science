git_url:		https://jeremysamuelson@bitbucket.org/jeremysamuelson/data_science.git

project overview: 	Building a classification model to detect fraudulent transactions for a financial services client.

thought process: 	The bulk of the work has been spent transforming the data and engineering inputs for the classification
			algorithms.  The transaction data are being treated as a directed network, where the nodes are accounts
			and the transactions themselves are the edges.  Then several metrics on the nodes and edges were derived.
			These metrics served as the inputs for the chosen classifiers.

results:		Both Naive Bayes and Logistic Regression classifiers were implemented resulting in extremely high accuracy,
			precision, and recall.  The Logistic Regression classifier has slightly higher accuracy.  However, the Naive
			Bayes classifier is the one I would recommend to the client.  This is due to the fact that the incorrect
			classifications of the Logistic Regression classifier were false negatives, meaning it missed fraudulent
			transactions.  These models were both cross-validate using the stratified k-folds method, with k = 10.
			
issues:			No issues.

next steps:		Now that prototyping is done, next steps will be to implement this on a distributed system using Spark. If
			time permits, the code can be translated into Scala from Python for improved compatibility with Spark, more
			robust and maintainable code, and faster performance.
